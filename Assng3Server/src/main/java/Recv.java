import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;

import java.io.IOException;

public class Recv {

    private final static String QUEUE_NAME = "hello";

    public static void main(String[] argv) throws Exception {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        final MailService mailService = new MailService("echiparosie.1@gmail.com","AmazonAlexa");
        final FileService fileService = new FileService();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String message = new String(body, "UTF-8");
                ObjectMapper objectMapper = new ObjectMapper();
                DVD dvd = objectMapper.readValue(message, DVD.class);
                String dvdString = "Title: " + dvd.getTitle() + ", Year: " + dvd.getYear() + ", Price: " + dvd.getPrice();
                System.out.println(" [x] Received '" + dvdString + "'");

                mailService.sendMail("echiparosie.1@gmail.com", "New album released!", dvdString);
                mailService.sendMail("istratevlad1311@gmail.com", "New album released!", dvdString);
                mailService.sendMail("brais.alexandra@yahoo.com", "New album released!", dvdString);
                fileService.writeToFile(dvdString);
            }
        };
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }
}
